package com.assignment.project.UI;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.assignment.project.DataBase.DataOperationHandler;
import com.assignment.project.DataBase.DbGoodsinfoListDataObject;
import com.assignment.project.R;

/**
 * Created by Pratishtha on 3/13/2016.
 */
public class AddGoodInBasket extends AppCompatActivity implements View.OnClickListener {
    Spinner item_spinner;
    Button save;
    EditText quantity;
    String itemSelected;
    int itemPosition;
    // price array
    String[] price_array = new String[]{
            "95",//
            "2.10",
            "1.30",
            "73"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_good_item);
// initialize activity
        initialize();

    }

    private void initialize() {

// button to save item in db
        save = (Button) findViewById(R.id.save_basket_button);
        save.setOnClickListener(this);
// spinner to select item 
        item_spinner = (Spinner) findViewById(R.id.item_spinner);
// edit text to get quantity of item
        quantity = (EditText) findViewById(R.id.edit_text_item);
// function to set spinner 
        setspinner(R.array.items_array, item_spinner);


    }

    private void setspinner(final int items_array, Spinner item_spinner) {
        ArrayAdapter<CharSequence> item_adapter = ArrayAdapter.createFromResource(this,
                items_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        item_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        item_spinner.setAdapter(item_adapter);
        item_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                itemSelected = parent.getSelectedItem().toString();
                itemPosition = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.save_basket_button:
                if (!quantity.getText().toString().equalsIgnoreCase("")) {
// save all values in db
                    SaveDataInDb(v);
                } else {
                    Snackbar.make(v, "Kindly fill quantity to proceed", Snackbar.LENGTH_SHORT).show();
                }

                break;
        }


    }

    private void SaveDataInDb(View v) {
        DbGoodsinfoListDataObject dbGoodsinfoListDataObject = new DbGoodsinfoListDataObject();
        dbGoodsinfoListDataObject.setGoodsTitle(itemSelected);
        dbGoodsinfoListDataObject.setGoodsCurrency("GBP");
        dbGoodsinfoListDataObject.setGoodsQuantity(quantity.getText().toString());
        dbGoodsinfoListDataObject.setGoodsPrice(getAmount(quantity.getText().toString()));
        DataOperationHandler db = DataOperationHandler.getInstance(AddGoodInBasket.this);
        db.insertGoodsInfoListDataObject(dbGoodsinfoListDataObject);
        Snackbar.make(v, "Data has been saved.", Snackbar.LENGTH_SHORT).show();
    }

    // get total price per item by using formula
// total price(single item in GBP)=  quantity enterd by user * //price available in price list 
    private String getAmount(String amount) {
        System.out.println("Android :" + price_array);
        Double value = Double.parseDouble(price_array[itemPosition]) * Double.parseDouble(amount);
        return "" + value;
    }
}
