package com.assignment.project.Config;

import android.content.Context;
import android.content.SharedPreferences;

import com.assignment.project.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Pratishtha on 3/13/2016.
 */
public class CommonClass {
// url to fetch currency list from server
    public static String url = "http://api.fixer.io/latest?base=GBP";
    public static String SfPrefName = "Json";
    public static String SfPrefKeyName = "JsonResponse";
    public static String AUD = "AUD";
    public static String BGN = "BGN";
    public static String BRL = "BRL";
    public static String CAD = "CAD";
    public static String CHF = "CHF";
    public static String CNY = "CNY";
    public static String CZK = "CZK";
    public static String DKK = "DKK";
    public static String HKD = "HKD";
    public static String HRK = "HRK";
    public static String HUF = "HUF";
    public static String IDR = "IDR";
    public static String ILS = "ILS";
    public static String INR = "INR";
    public static String JPY = "JPY";
    public static String KRW = "KRW";
    public static String MXN = "MXN";
    public static String MYR = "MYR";
    public static String NOK = "NOK";
    public static String NZD = "NZD";
    public static String PHP = "PHP";
    public static String PLN = "PLN";
    public static String RON = "RON";
    public static String RUB = "RUB";
    public static String SEK = "SEK";
    public static String SGD = "SGD";
    public static String THB = "THB";
    public static String TRY = "TRY";
    public static String USD = "USD";
    public static String ZAR = "ZAR";
    public static String EUR = "EUR";
// function to parse json
    public static List<HashMap<String, String>> parseJson(JSONObject jsonObject, Context context) {
        List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        String[] currencyArray = context.getResources().getStringArray(R.array.currency_array);
//        System.out.println("Android : VolleyResponse :" + response);
        try {
            String rates = jsonObject.getString("rates");
            JSONObject ratesJsonObject = new JSONObject(rates);
            for (int i = 0; i < currencyArray.length; i++) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(currencyArray[i], ratesJsonObject.getString(currencyArray[i]));
                list.add(map);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

// function to save json in sharedpreference
    public static void saveJsonInPrefrences(String response, Context context) {

        SharedPreferences sf = context.getSharedPreferences(CommonClass.SfPrefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sf.edit();
        edit.putString(CommonClass.SfPrefKeyName, response);
        edit.commit();

    }

// function to get json from sharedpreference
    public static String getJsonInPreferences(Context context) {
        String Value = "";
        SharedPreferences sf = context.getSharedPreferences(CommonClass.SfPrefName, Context.MODE_PRIVATE);
        Value = sf.getString(CommonClass.SfPrefKeyName, "");
        return Value;
    }


}