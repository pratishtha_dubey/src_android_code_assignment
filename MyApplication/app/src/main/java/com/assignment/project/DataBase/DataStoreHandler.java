package com.assignment.project.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataStoreHandler extends SQLiteOpenHelper {

    Context context;

    public DataStoreHandler(Context context, String name,
                            SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /*
    entries for table Goodsinfo
     */
    public static final String GOODSINFO_LIST_TABLE_NAME = "goodsinfo";
    public static final String GOODSINFO_COLUMN_ID = "id";
    public static final String GOODSINFO_GOOD_TITLE = "title";
    public static final String GOODSINFO_GOOD_CURRENCY = "currency";
    public static final String GOODSINFO_GOODS_QUANTITY = "quantity";
    public static final String GOODSINFO_GOODS_PRICE = "pirice";

    public static final String[] allColGoodsInfoList = {GOODSINFO_COLUMN_ID,
            GOODSINFO_GOOD_TITLE, GOODSINFO_GOOD_CURRENCY, GOODSINFO_GOODS_QUANTITY, GOODSINFO_GOODS_PRICE};


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_APPIFNO_TABLE = "CREATE TABLE "
                + GOODSINFO_LIST_TABLE_NAME + " (" + GOODSINFO_COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, " + GOODSINFO_GOOD_TITLE
                + " TEXT UNIQUE, " + GOODSINFO_GOOD_CURRENCY + " TEXT, "
                + GOODSINFO_GOODS_QUANTITY + " TEXT, " + GOODSINFO_GOODS_PRICE + " TEXT " + ")";
        db.execSQL(CREATE_APPIFNO_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GOODSINFO_LIST_TABLE_NAME);

        onCreate(db);

    }


    /**
     * Creates a empty database on the system and rewrites it with your own
     * database.
     */
//    public void createDataBase() throws IOException {
//
//        boolean dbExist = checkDataBase();
//
//        if (dbExist) {
//            // do nothing - database already exist
//
//        } else {
//
//            // By calling this method and empty database will be created into
//            // the default system path
//            // of your application so we are gonna be able to overwrite that
//            // database with our database.
//            this.getReadableDatabase();
//
//            try {
//
//                copyDataBase();
//
//            } catch (IOException e) {
//
//                throw new Error("Error copying database");
//
//            }
//        }
//    }


    /**
     * Check if the database already exist to avoid re-copying the file each
     * time you open the application.
     *
     * @return true if it exists, false if it doesn't
    //     */
//    private boolean checkDataBase() {
//
//        SQLiteDatabase checkDB = null;
//
//        try {
//            String myPath = getDbPath() + DB_NAME;
//            checkDB = SQLiteDatabase.openDatabase(myPath, null,
//                    SQLiteDatabase.OPEN_READONLY);
//
//        } catch (SQLiteException e) {
//
//            // database does't exist yet.
//
//        }

//        if (checkDB != null) {
//
//            checkDB.close();
//
//        }
//
//        return checkDB != null ? true : false;
//    }


    /**
     * Copies your database from your local assets-folder to the just created
     * empty database in the system folder, from where it can be accessed and
     * handled. This is done by transfering bytestream.
     */
//    private void copyDataBase() throws IOException {
//
//        try {
//            // Open your local db as the input stream
//            InputStream myInput = context.getAssets().open(DB_NAME);
//
//            // Path to the just created empty db
//            String outFileName = getDbPath() + DB_NAME;
//
//            // Open the empty db as the output stream
//            OutputStream myOutput = new FileOutputStream(outFileName);
//
//            // transfer bytes from the inputfile to the outputfile
//            byte[] buffer = new byte[1024];
//            int length;
//            while ((length = myInput.read(buffer)) > 0) {
//                myOutput.write(buffer, 0, length);
//            }
//
//            // Close the streams
//            myOutput.flush();
//            myOutput.close();
//            myInput.close();
//        } catch (Exception e) {
//
//        }
//    }
//
//    private String getDbPath() {
//        String DB_PATH = "/data/data/" + context.getPackageName()
//                + "/databases/";
//        return DB_PATH;
//    }
}
