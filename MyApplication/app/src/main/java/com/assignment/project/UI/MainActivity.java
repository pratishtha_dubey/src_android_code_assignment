package com.assignment.project.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.assignment.project.Config.CommonClass;
import com.assignment.project.CustomAdapter.GoodsAdapter;
import com.assignment.project.DataBase.DataOperationHandler;
import com.assignment.project.DataBase.DbGoodsinfoListDataObject;
import com.assignment.project.R;

import java.util.List;

// Class to show list of basket
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button checkout;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    List<DbGoodsinfoListDataObject> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // function to initiate activity component
        initiateActivity();
        // Do volley request to fetch currency list from server.

        DoVolleyRequestForCurrencyList(MainActivity.this);

    }

    private void initiateActivity() {
        // button to add item in basket
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        // button to see price in diff currency
        checkout = (Button) findViewById(R.id.check_out_button);
        checkout.setOnClickListener(this);

        // recyclerview to show list of item in busket.
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setAdapter();

    }

    private void setAdapter() {
        if (list != null)
            list.clear();
        // initiate database class
        DataOperationHandler db = DataOperationHandler.getInstance(MainActivity.this);
        //fetch list of all item from db
        list = db.getAllGoodsInfoListDataObject();
        // if size of list is grater than zero,set adapter
        if (list.size() > 0) {
            GoodsAdapter goodsAdapter = new GoodsAdapter(list);
            recyclerView.setAdapter(goodsAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // function to get currency list from server.
    public void DoVolleyRequestForCurrencyList(final Context context) {
        System.out.println("Android :DoVolleyRequestForCurrencyList ");
        // show progress dialog while fetching data from server
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Fetching data from server.Please wait..");
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(context);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CommonClass.url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // cancel progress dialog as response is here. 
                        progressDialog.cancel();
                        System.out.println("Android : " + response);
                         // save response in sharedpreference for buffer.
                        CommonClass.saveJsonInPrefrences(response, MainActivity.this);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // cancel progress dialoge and show error to user.
                progressDialog.cancel();
                System.out.println("Android : " + error.toString());
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.fab:
                // show activity to add item in basket
                Intent additem = new Intent(MainActivity.this, AddGoodInBasket.class);
                startActivity(additem);
                break;
            case R.id.check_out_button:
                // get total item from db
                DataOperationHandler dataOperationHandler = DataOperationHandler.getInstance(MainActivity.this);
                List<DbGoodsinfoListDataObject> list = dataOperationHandler.getAllGoodsInfoListDataObject();
                if (list.size() > 0) {
                    // get total amount in double and send to currency display activity
                    Intent intent = new Intent(MainActivity.this, CurrencyListDisplay.class);
                    intent.putExtra("value", getTotalAmount());
                    startActivity(intent);
                } else {

                    Snackbar.make(v, "Kindly save a item in basket.", Snackbar.LENGTH_SHORT).show();
                }
                break;
        }

    }

    // to get sum of all item price saved in db
    private double getTotalAmount() {
        double value = 0.0;
        DataOperationHandler dataOperationHandler = DataOperationHandler.getInstance(MainActivity.this);
        List<DbGoodsinfoListDataObject> list = dataOperationHandler.getAllGoodsInfoListDataObject();
        for (int i = 0; i < list.size(); i++) {
            value = value + Double.parseDouble(list.get(i).getGoodsQuantity());
        }

        return value;
    }


}
