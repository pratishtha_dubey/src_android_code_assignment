package com.assignment.project.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DataOperationHandler {

    SQLiteDatabase db;

    DataStoreHandler dshandler;

    private static DataOperationHandler dh = null;

    private static final String DATABASE_NAME = "code";

    private DataOperationHandler() {
    }

    public static DataOperationHandler getInstance(Context ctx) {
        if (dh == null) {
            dh = new DataOperationHandler();
            dh.getDataOperationHandler(ctx);
        }
        return dh;
    }

    private void getDataOperationHandler(Context context) {
        try {
            dshandler = new DataStoreHandler(context, DATABASE_NAME, null, 1);
            db = dshandler.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
            close();
        }
    }

    private void close() {
        try {
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            dshandler.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * GoodsInfo List functions
     */
    public long insertGoodsInfoListDataObject(DbGoodsinfoListDataObject obj) {
        ContentValues values = new ContentValues();
        values.put(DataStoreHandler.allColGoodsInfoList[1], obj.getGoodsTitle());
        values.put(DataStoreHandler.allColGoodsInfoList[2], obj.getGoodsCurrency());
        values.put(DataStoreHandler.allColGoodsInfoList[3], obj.getGoodsQuantity());
        values.put(DataStoreHandler.allColGoodsInfoList[4], obj.getGoodsPrice());

        long insertId = db.insert(DataStoreHandler.GOODSINFO_LIST_TABLE_NAME,
                null, values);
        return insertId;

    }

    /*
     * update GoodsInfo List
     */
    public int updateGoodsInfoListDataObject(DbGoodsinfoListDataObject obj) {
        ContentValues values = new ContentValues();
        values.put(DataStoreHandler.allColGoodsInfoList[1], obj.getGoodsTitle());
        values.put(DataStoreHandler.allColGoodsInfoList[2], obj.getGoodsCurrency());
        values.put(DataStoreHandler.allColGoodsInfoList[3], obj.getGoodsQuantity());
        values.put(DataStoreHandler.allColGoodsInfoList[4], obj.getGoodsPrice());


        int updated = db.update(DataStoreHandler.GOODSINFO_LIST_TABLE_NAME,
                values,
                DataStoreHandler.allColGoodsInfoList[0] + "=" + obj.getRowId(),
                null);
        return updated;


    }

    /*
     * delete function for GoodsInfo
     */
    public int deleteGoodsInfoListDataObject(DbGoodsinfoListDataObject object) {
        long id = object.getRowId();
        int rowAffected = db.delete(DataStoreHandler.GOODSINFO_LIST_TABLE_NAME,
                DataStoreHandler.allColGoodsInfoList[0] + " = " + id, null);

        return rowAffected;
//        return 0;
    }

    /*
     * Getting all column of GoodsInfo list
     */
    public List<DbGoodsinfoListDataObject> getAllGoodsInfoListDataObject() {
        List<DbGoodsinfoListDataObject> dataArr = new ArrayList<DbGoodsinfoListDataObject>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DataStoreHandler.GOODSINFO_LIST_TABLE_NAME, null);
        if (cursor != null) {
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    DbGoodsinfoListDataObject data = cursorToContactListDataObject(cursor);
                    dataArr.add(data);
                    cursor.moveToNext();
                }
            }

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return dataArr;
    }

    /*
     * Getting cursor list of GoodsInfo list
     */
    private DbGoodsinfoListDataObject cursorToContactListDataObject(Cursor cursor) {
        DbGoodsinfoListDataObject data = new DbGoodsinfoListDataObject();
        data.setRowId(cursor.getLong(0));
        data.setGoodsTitle(cursor.getString(1));
        data.setGoodsCurrency(cursor.getString(2));
        data.setGoodsQuantity(cursor.getString(3));
        data.setGoodsPrice(cursor.getString(4));
        return data;
    }

}
