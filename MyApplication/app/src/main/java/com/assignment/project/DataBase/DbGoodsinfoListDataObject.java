
package com.assignment.project.DataBase;

public class DbGoodsinfoListDataObject {
    private long rowId = -1;
    private String price = "";
    private String goodsTitle = "";

    private String goodsCurrency = "";

    private String goodsQuantity = "";


    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getGoodsTitle() {
        return goodsTitle;
    }

    public void setGoodsTitle(String title) {
        this.goodsTitle = title;
    }


    public String getGoodsCurrency() {
        return goodsCurrency;
    }

    public void setGoodsCurrency(String goodsCurrency) {
        this.goodsCurrency = goodsCurrency;
    }

    public String getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setGoodsQuantity(String goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }

    public String getGoodsPrice() {
        return price;
    }

    public void setGoodsPrice(String price) {
        this.price = price;
    }

}
