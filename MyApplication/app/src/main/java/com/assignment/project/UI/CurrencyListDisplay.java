package com.assignment.project.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.assignment.project.Config.CommonClass;
import com.assignment.project.CustomAdapter.CurrencyAdapter;
import com.assignment.project.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Pratishtha on 3/13/2016.
 */
public class CurrencyListDisplay extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
// fetch total value from intent
        Double value = getIntent().getDoubleExtra("value", 0.0);
        setContentView(R.layout.currency_list_activity);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        JSONObject jsonObject = null;
        try {
            String response = CommonClass.getJsonInPreferences(CurrencyListDisplay.this);
            jsonObject = new JSONObject(response);

        } catch (JSONException e) {
            e.printStackTrace();

        }
        List<HashMap<String, String>> list = CommonClass.parseJson(jsonObject, CurrencyListDisplay.this);

        // specify an adapter
        CurrencyAdapter currencyAdapter = new CurrencyAdapter(value, list);
        recyclerView.setAdapter(currencyAdapter);


    }
}
