package com.assignment.project.CustomAdapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.project.DataBase.DataOperationHandler;
import com.assignment.project.DataBase.DbGoodsinfoListDataObject;
import com.assignment.project.R;

import java.util.List;

/**
 * Created by Samasya on 3/13/2016.
 */
public class GoodsAdapter extends RecyclerView.Adapter<GoodsAdapter.ViewHolder> {
    private List<DbGoodsinfoListDataObject> mDataset;

    public GoodsAdapter(List<DbGoodsinfoListDataObject> list) {
        this.mDataset = list;

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        Context context;
        public TextView currency, itemName, quantity;

        public ViewHolder(View v) {
            super(v);
            context = v.getContext();
            currency = (TextView) v.findViewById(R.id.currency);
            itemName = (TextView) v.findViewById(R.id.title);
            quantity = (TextView) v.findViewById(R.id.quantity);
            ImageView delete = (ImageView) v.findViewById(R.id.delete);
            delete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.delete:
                    showDialougeForConfirmation(v.getContext(),getAdapterPosition());


                    break;
            }
        }
    }

    private void showDialougeForConfirmation(final Context context, final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

// 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                DataOperationHandler db = DataOperationHandler.getInstance(context);
                db.deleteGoodsInfoListDataObject(mDataset.get(position));
                mDataset.remove(position);
                notifyItemRemoved(position);


            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        builder.create();
        builder.show();
    }



    // Create new views (invoked by the layout manager)
    @Override
    public GoodsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.goods_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.quantity.setText(mDataset.get(position).getGoodsQuantity());
        holder.itemName.setText(mDataset.get(position).getGoodsTitle());
        holder.currency.setText(mDataset.get(position).getGoodsCurrency());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}

